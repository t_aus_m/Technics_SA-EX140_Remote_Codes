| Function             | Key(s)                 | Code                                                                                              |
| -------------------- | ---------------------- | ------------------------------------------------------------------------------------------------- |
| Power Receiver       | `Tuner`followed by `⏻` | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x40040500BCB9","DataLSB":"0x220A0003D9D","Repeat":0}` |
| Power CD             | `CD`followed by `⏻`    | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x40040550BCE9","DataLSB":"0x220A00A3D97","Repeat":0}` |
| Power off everything | `Audio ⏻`              | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x40040538FCC1","DataLSB":"0x220A01C3F83","Repeat":0}` |
| Source TV            | `TV`                   | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x40040500F9FC","DataLSB":"0x220A0009F3F","Repeat":0}` |
| Source VCR           | `VCR`                  | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x40040500797C","DataLSB":"0x220A0009E3E","Repeat":0}` |
| Source Tuner         | `TUNER`                | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x40040500494C","DataLSB":"0x220A0009232","Repeat":0}` |
| Source Tape/Monitor  | `TAPE MONITOR`         | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x400405005550","DataLSB":"0x220A000AA0A","Repeat":0}` |
| Volume up            | `VOLUME +`             | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x400405000401","DataLSB":"0x220A0002080","Repeat":0}` |
| Volume down          | `VOLUME -`             | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x400405008481","DataLSB":"0x220A0002181","Repeat":0}` |
| Mute                 | `MUTING`               | `{"Protocol":"PANASONIC","Bits":48,"Data":"0x400405004C49","DataLSB":"0x220A0003292","Repeat":0}` |